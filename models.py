from django.db import models


class Sequence(models.Model):
    text = models.CharField(max_length=12)
    audio = models.FileField(null=True)

    def __str__(self):
        return self.text

