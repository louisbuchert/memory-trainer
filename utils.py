from django.conf import settings
from pydub import AudioSegment
import string
import random


def number_generator(size=12, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def audio_generator(sequence):
    audio_list = 0
    for digit in reversed(sequence):
        path = str(settings.BASE_DIR) + '/memtrainer/media/' + str(digit) + '.mp3'
        audio_list += AudioSegment.from_mp3(path)
    audio_list.export(settings.MEDIA_ROOT + '/sequence.mp3', format = 'mp3')

