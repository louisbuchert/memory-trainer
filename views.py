from django.shortcuts import render, get_object_or_404
from django.core.files import File
from django.conf import settings
from random import randint
from pathlib import Path
from .models import Sequence
from .utils import number_generator, audio_generator


def loading(request):
    return render(request, 'memtrainer/loading.html')


def exercise(request):
    text = number_generator(randint(1, 12))
    audio_generator(text)
    path = Path(str(settings.MEDIA_ROOT) + '/sequence.mp3')
    with path.open(mode='rb') as f:
        sequence, created = Sequence.objects.get_or_create(id=1)
        sequence.audio = File(f, name='sequence.mp3')
        sequence.text = text
        sequence.save()

    context = {'sequence': sequence}
    return render(request, 'memtrainer/exercise.html', context)

