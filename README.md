Django application to train memory.

User shall enter in the reverse order the number he heard.

To install the app in a Django environment, edit the site files (not in memtrainer folder):

File urls.py:
```
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("memtrainer/", include("memtrainer.urls"))
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static("/", document_root=settings.BASE_DIR)
```

In file settings.py:
```
STATIC_URL = 'static/'

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
```

